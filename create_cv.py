#!/usr/bin/env python3

import subprocess
import shlex
import textwrap
import os
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

parser = ArgumentParser(
    formatter_class=RawDescriptionHelpFormatter,
    description=textwrap.dedent('''CV generator: \
    input.md --(pandoc)--> content.tex
    content.tex --(template.tex)--> standalone.tex
    standalone.tex --(lualatex)--> output.pdf
    '''),
    epilog=textwrap.dedent('''Dependencies:
    * pandoc
    * lualatex
    * latex template (--template)
    * markdown input (--in)
    ''')
)

parser.add_argument('--input',
                    default='cv_andre_2021_tech.md',
                    help='markdown file input; Default: %(default)s'
                    )
parser.add_argument('--tmplatex',
                    default='tmp.tex',
                    help='markdown file input; Default: %(default)s'
                    )
parser.add_argument('--template',
                    default='cv_template.tex',
                    help='lualatex template; Default: %(default)s'
                    )
parser.add_argument('--output',
                    default='cv.pdf',
                    help='markdown file input; Default: %(default)s'
                    )

parser.add_argument('--debug',
                    default=False,
                    type=bool,
                    help='if true keep tmp files; Default: %(default)s'
                    )

args = parser.parse_args()
print(args)

def purge_tmp():
    tmp_files = [args.tmplatex.replace('.tex', ext) for ext in
                 ['.tex', '.log', '.aux', '.log']]
    for file in os.listdir():
        if file in tmp_files:
            os.remove(file)


pandoc_cmd = 'pandoc -f markdown-auto_identifiers \
              -t latex {input} -o {output}'.format(input=args.input,
                                                   output=args.tmplatex)
pandoc_cmd = shlex.split(pandoc_cmd)
subprocess.call(pandoc_cmd)

with open(args.tmplatex, 'r') as tmplatex:
    tmplatex_content = tmplatex.read()

with open(args.template, 'r') as template:
    template_content = template.read()

# replace empty line in template with tmplatex_content
standalone_latex = template_content.replace('\n\n',
                                            '\n' + tmplatex_content + '\n')

# reuse tmplatex to write standalone latex content
with open(args.tmplatex, 'w') as tmplatex:
    tmplatex_content = tmplatex.write(standalone_latex)

lualatex_cmd = shlex.split('lualatex {}'.format(args.tmplatex))
# print(lualatex_cmd)
try:
    subprocess.call(lualatex_cmd)
    # lualatex names the output pdf the same as input with .tex -> .pdf
    # hence need to change it to the correct output filename
    os.rename(args.tmplatex.replace('.tex', '.pdf'), args.output)
    print('Created PDF {}'.format(args.output))
    if args.debug is False:
        purge_tmp()
except Exception as e:
    print('An error has occurred in the PDF creation')
