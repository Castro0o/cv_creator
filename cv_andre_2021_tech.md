
# ANDRÉ CASTRO 

**(he/they)\
Born in Lisbon, Portugal, 1983.\
Based in Rotterdam.**

**<andrecastro83@gmail.com>\
+31 (0)6 84660870**


**<http://artserver.org/>**\
**<https://github.com/andrecastro0o>**\
**<https://www.linkedin.com/in/andre-castr0/>**\





## WORK POSITIONS

**2022-present - Data Manager at [Ruisdael Observatory](https://ruisdael-observatory.nl/), Technical University Delft**

**2022-2022 - Senior Data Scientist at [Ontotext](https://www.ontotext.com/)**

**2020-2021 - Linked Data Engineer at [TIB](https://www.tib.eu/).**\
Implementation of technical infrastructure for knowledge graphs and scientific data management systems in [confIDent](https://projects.tib.eu/en/confident/contact/), [NFDI4Chem](https://nfdi4chem.de/), [NFDI4Culture](https://nfdi4culture.de/) and [Sonderforschungsbereich](https://www.sfb1368.uni-hannover.de/de/) projects. Contributions to ontologies in the chemistry, engineering and academic domains domains. Automated deployment and testing pipelines TIB’s [Ontology Look-up Service](https://service.tib.eu/ts4tib/index).

**2019-2020 - Python Developer at [TVT Media/DMC](https://www.tvt.media/)**

**2017-2020 - Core Tutor at [Master of Experimental Publishing](http://xpub.nl/) - [Piet Zwart Institute](http://www.pzwart.nl/)**

**2014-2020 - Tutor at Willem de Kooning Academy - [Publication Station](http://publicationstation.wdka.hro.nl/wiki/)**

**2014-2017 - Technical Tutor at [Master Media Design and Communication](http://pzwiki.wdka.nl/mediadesign/Main_Page)**  - [Piet Zwart Institute](http://www.pzwart.nl/).

**2014-2015 - Software developer for hybrid-publishing workflows at [Institute of Network Cultures](http://networkcultures.org/)**.


## CONSULTANCY 

**2022- [Tailbone](https://tailboneshop.com/) (sex-positive and queer shop) - collaborator.**\ IT consultancy on logistics automation.

**2019 - [Étude #1: On Recitation](http://wiki.artserver.org/index.php/%C3%89tude1:_On_Recitation)**  by Rana Hamadeh. For [Sonic Acts 2019](https://2019.sonicacts.com/).
	
**2019 - [Post Opera exhibition](https://www.tentrotterdam.nl/en/show/next-up-post-opera/)** - Exhibition light & sound playback network implementation, [Tent Rotterdam](https://www.tentrotterdam.nl/).

**2017 - Rana Hamadeh's [The Ten Murders of Josephine](http://wiki.artserver.org/index.php/The_Ten_Murders_of_Josephine)** - Network Design & consultancy, [Witte de With](https://www.wdw.nl/en/), [Lafayette Anticipations](https://www.lafayetteanticipations.com/en).

**2017 - Rana Hamadeh's The Ten Murders of Josephine - *first iteration*** - live sound processing system, [Countour Biennial](http://contour8.be/), Mechelen. 

**2016 - Lawrence Abu Hamdan's [Humming Bird Clock](http://hummingbirdclock.info/)** - Back end programming, [Liverpool Biennial](https://biennial.com/).

**2014 - [Rotterdam Fabric Map](http://wiki.artserver.org/index.php/Rotterdam_Map)**
- for Rotterdam’s Municipality, in collaboration with GROUP A - studio
for architecture.

**2013 - Winter Night’s Copyright Fairytale** - coordination and
soundtrack composition for the extravagant conference on free culture,
as a part of **[FREE!?](http://freeculture.info)**. Het
Nieuwe Instituut, Rotterdam.\


## PUBLISHING PROJECTS

**2016-2018 - [Warp Weft Memory](http://warpweftmemory.net/)** Semantic Mediawiki and server administration. Author: [Renée Turner](http://www.fudgethefacts.com/).

**2016 - [A Radiated Book](http://wiki.artserver.org/index.php/Mondoth%C3%A8que:_A_Radiated_Book)**. Part of editorial team. Publisher: [Constant](http://constant.be/).

**2014-2016 - [Beyond Social - Platform Investigating Social Art and Design](http://wiki.artserver.org/index.php/Beyond_Social)**. Development of publishing workflow. Publisher: Willem de Kooning Academy.

----

## WORKSHOP LEADER

**2014-2016 - [Bibliotecha workshop](http://bibliotecha.info/workshop)** - [Impakt](https://www.impakt.nl/), Utrech. [Radical Networks](https://radicalnetworks.org/), New York. [Jan van Eyck Academie](https://www.janvaneyck.nl), Maastricht. [AMRO Festival](https://radical-openness.org/en), Linz.

**2011 - Tactile Noise** workshop, [WORM](http://www.worm.org/),
Rotterdam. [LCD](http://lcd.guimaraes2012.pt/), Guimarães. [ISEA 2011](http://isea2011.sabanciuniv.edu/) , Istanbul.  [Oficinas do 
Convento](http://www.oficinasdoconvento.com/), Montemor-o-Novo.
[Performance space](http://www.performancespace.org/), London. [xDA
hacklab](http://xdatelier.org/), Coimbra.

---

## EDUCATION 

**2011-2013 - MA Master Media Design & Communication,** [Piet Zwart
Institute](http://pzwart.wdka.nl), Willem de Kooning Academie,
Hogeschool Rotterdam.

**2004-2007 - BA SonicArts**, Middlesex
University, London.

**2009-2010 - Internship, NK**, Berlin.

**2003-2004 - Music technologies program, Restart**, Lisbon.

## CERTIFICATES

* Class B Diver's license

* CPR (2017)

* Scuba-diving Divemaster (PADI)


## LANGUAGE PROFICIENCY
* **English:** Fluent

* **Portuguese:** Native

* **Spanish:** Conversational

* **Dutch:** Basic

* **French:** Basic

* **German:** Basic

---

## TECHNOLOGICAL SUMMARY

* **Programming Languages:** 

	* Python (RDFLib, NLTK, Flask, SQLAlchemy, Jinja2, pytest, pandas, Weasyprint, mwclient)

	* HTML, CSS, JavaScript

	* Bash

* **Databases:** MySQL, MongoDB.

* **Knowledge Graphs/Ontology building**: RDF, RDFS, OWL, SKOS, SPARQL, SHACL, Apache Jena suite, ROBOT OBO Tool, Protégè, Ontology Lookup Service, TARQL.

* **Software:**
    * Data & Knowledge Management: Apache Airflow, CKAN, Fuseki, Mediawiki (with Semantic Mediawiki and Wikibase extensions)

	* Provisioning/system: Gitlab CI, Github Actions, Ansible, Vagrant, Docker, Docker Compose, nginx, Apache2, Git, Tinc VPN 
	
	* Other: tinc-VPN, ffmpeg, Asterisk, LaTeX, Pandoc

* **Operating Systems:** Debian, Ubuntu, Mac OS.


**Areas of expertise:** knowledge management, knowledge graphs, data engineeriog, ontology development, digital archives, devops, tutoring, project coordination.

* **Ontology repositories:**
	* [DFG-Fachsystematik Ontology](https://github.com/tibonto/DFG-Fachsystematik-Ontology)  

	* [Academic Event Ontology](https://github.com/tibonto/aeon)

	* [Machine and Tool Ontology](https://github.com/tibonto/mato)

* **Software repositories:**

	* [ontology2smw](https://github.com/TIBHannover/ontology2smw) - python cli to automate an RDF ontology import into Semantic Mediawiki.

	* [Semantics Prototypes](https://github.com/NFDI4Chem/Semantics-Prototypes) - Jupyter notebooks for integrating semantics in [NFID4Chem - Chemistry Consortium](https://www.nfdi4chem.de/)

