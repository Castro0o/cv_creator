# CV generator

```
usage: create_cv.py [-h] [--input INPUT] [--tmplatex TMPLATEX]
                    [--template TEMPLATE] [--output OUTPUT] [--debug DEBUG]

CV generator:     input.md --(pandoc)--> content.tex
    content.tex --(template.tex)--> standalone.tex
    standalone.tex --(lualatex)--> output.pdf

optional arguments:
  -h, --help           show this help message and exit
  --input INPUT        markdown file input; Default: cv_andre_2021_tech.md
  --tmplatex TMPLATEX  markdown file input; Default: tmp.tex
  --template TEMPLATE  lualatex template; Default: cv_template.tex
  --output OUTPUT      markdown file input; Default: cv.pdf
  --debug DEBUG        if true keep tmp files; Default: False

Dependencies:
    * pandoc `sudo apt install pandoc`
    * lualatex `sudo apt install texlive texlive-luatex`
    * [enumitem](https://www.ctan.org/pkg/enumitem) `enumitem.sty`
    * latex template (--template)
    * markdown input (--in)
```